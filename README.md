### EMAM Autopilot model showcase
* requirements:
    * `Windows 64 bit`
* usage:
    * go to `scripts` folder
    * run `main.bat` __from there__ i.e. the script assumes that the working directory is `scripts`
* what's under the hood:
    * JDK: `jdk`
    * Octave: `octave-4.2.1`
    * GCC toolchain: `mingw64`
    * Apache Tomcat web server: `apache-tomcat-9.0.5`
    * EMAM Autopilot model: `model`
    * EMAM2CPP generator: `emam2cpp.jar`
    * Batch scripts that glue all this stuff together: `scripts`